#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
    int Mode = 0;
    int NumOne = 0;
    int NumTwo = 0;
    int Answer;

    printf("Please select a mode: (1:Addition 2:Subtraction 3:Multiply 4:Divide) ");
    scanf("%d", &Mode);

    printf("Please enter the first number: ");
    scanf("%d", &NumOne);

    printf("Please enter the second number: ");
    scanf("%d", &NumTwo);
    if(Mode == 1){
        Answer = NumOne + NumTwo;
    }
    if(Mode == 2){
        Answer = NumOne - NumTwo;
    }
    if(Mode == 3){
        Answer = NumOne * NumTwo;
    }
    if(Mode == 4){
        Answer = NumOne / NumTwo;
    }
    printf("This is your answer: %d", Answer);
}