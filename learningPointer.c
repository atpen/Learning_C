#include <stdio.h>

int main () {

   int var1;
   char var2[10];
    for(int i = 0; i < 10; i++){
        var2[i] = i+6;
    }
   int * addressVar1 = &var1;

   printf("Address of var1 variable: %x\n", addressVar1);
   printf("Address of var2 variable: %x\n", &var2  );

   return 0;
}